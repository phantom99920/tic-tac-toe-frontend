import {Component, OnDestroy, OnInit} from '@angular/core';
import {GameService} from "../game.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Movement} from "../models/movement";
import {SharedService} from "../shared/shared.service";
import {connectableObservableDescriptor} from "rxjs/internal/observable/ConnectableObservable";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit, OnDestroy {

  squares: any[] = [];
  xIsNext: boolean = true;
  winner: string ='';
  player1Name: string ='';
  player2Name: string ='';
  moves: Movement[] = [];
  currentPlayer: string = '';
  boardId: string = '';
  playerNum1 = "1";
  playerNum2 = "2";
  currentPlayerNum = "1";
  locked = false;
  temporalMoves: Movement[] = [];
  temporalSquare: any[] = [];
  getTemporalArray: Subscription | undefined;
  getFirstMoveSubscription: Subscription | undefined;

  constructor(
    private gameService: GameService,
    private router: Router,
    private shared: SharedService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {

    this.init();
    this.getTemporalArray =this.shared.getTempArray().subscribe(resp =>{
      this.temporalMoves = resp;
      console.log(this.temporalMoves);
      console.log(this.moves)
      if(JSON.stringify(this.temporalMoves)=== JSON.stringify(this.moves)){
        this.locked= false;
      } else{
        //this.moves=this.temporalMoves;
        console.log(this.locked);
        this.locked = true;
        this.temporalSquare = Array(9).fill(null);
        this.fillBoard(this.temporalMoves, this.temporalSquare )

      }


    })

  }

  init(){
    this.gameService.getCurrentGame().subscribe((response: any) => {
      if(response[0]){
      if (Object.keys(response[0]).length > 0) {
        this.player1Name = response[0].player1Name;
        this.player2Name = response[0].player2Name;
        if (response[0].movements === null) {
          this.moves = [];
        } else {
          this.moves = response[0].movements;
        }
        this.boardId = response[0].id;
        this.getFirstMoveSubscription = this.shared.getStartingPlayer().subscribe(resp => {
          this.currentPlayerNum = resp
          console.log(resp)
          if (resp === "1") {
            this.currentPlayer = this.player1Name;
            this.xIsNext = true;
          } else if (resp === "2") {
            this.currentPlayer = this.player2Name;
            this.xIsNext = false;
          }
        })
        this.continueGame();
      } else {
        this.router.navigate(['../name'], {relativeTo: this.route})
      }
    }
    });
  }

  // newGame(): void {
  //   this.gameService.createGame({"player1Name":this.player1Name,"player2Name": this.player2Name}).subscribe((response: any)=>{
  //     this.squares = Array(9).fill(null);
  //     this.winner = '';
  //   })
  //
  //
  //   // this.xIsNext = true;
  //   this.currentPlayer= this.player1Name;
  //   this.currentPlayerNum = this.playerNum1;
  // }

  continueGame(): void {
    this.squares = Array(9).fill(null);
    this.fillBoard(this.moves, this.squares);

    let lastPlayer = "";
    if(this.moves.length >0) {
       lastPlayer = this.moves[this.moves.length-1].player;
    }
    if(lastPlayer ==="1" ){
      this.currentPlayer = this.player2Name;
      this.xIsNext = false;
      this.currentPlayerNum = "2"
    } else if(lastPlayer ==="2" ){
      this.currentPlayer = this.player1Name;
      this.xIsNext = true;
      this.currentPlayerNum = "1"
    }
    this.winner = '';

  }

  fillBoard(moves: Movement[], array: String[]) {
    for (let i = 0; i < Object.keys(moves).length; i++) {
      switch (true) {
        case (this.arrayEquals(moves[i].positions, [0, 0]) && moves[i].player === "1"):
          array[0] = 'X'
          break;
        case (this.arrayEquals(moves[i].positions, [0, 1]) && moves[i].player === "1"):
          array[1] = 'X'
          break;
        case (this.arrayEquals(moves[i].positions, [0, 2]) && moves[i].player === "1"):
          array[2] = 'X'
          break;
        case (this.arrayEquals(moves[i].positions, [1, 0]) && moves[i].player === "1"):
          array[3] = 'X'
          break;
        case (this.arrayEquals(moves[i].positions, [1, 1]) && moves[i].player === "1"):
          array[4] = 'X'
          break;
        case (this.arrayEquals(moves[i].positions, [1, 2]) && moves[i].player === "1"):
          array[5] = 'X'
          break;
        case (this.arrayEquals(moves[i].positions, [2, 0]) && moves[i].player === "1"):
          array[6] = 'X'
          break;
        case (this.arrayEquals(moves[i].positions, [2, 1]) && moves[i].player === "1"):
          array[7] = 'X'
          break;
        case (this.arrayEquals(moves[i].positions, [2, 2]) && moves[i].player === "1"):
          array[8] = 'X'
          break;
        case (this.arrayEquals(moves[i].positions, [0, 0]) && moves[i].player === "2"):
          array[0] = 'O'
          break;
        case (this.arrayEquals(moves[i].positions, [0, 1]) && moves[i].player === "2"):
          array[1] = 'O'
          break;
        case (this.arrayEquals(moves[i].positions, [0, 2]) && moves[i].player === "2"):
          array[2] = 'O'
          break;
        case (this.arrayEquals(moves[i].positions, [1, 0]) && moves[i].player === "2"):
          array[3] = 'O'
          break;
        case (this.arrayEquals(moves[i].positions, [1, 1]) && moves[i].player === "2"):
          array[4] = 'O'
          break;
        case (this.arrayEquals(moves[i].positions, [1, 2]) && moves[i].player === "2"):
          array[5] = 'O'
          break;
        case (this.arrayEquals(moves[i].positions, [2, 0]) && moves[i].player === "2"):
          array[6] = 'O'
          break;
        case (this.arrayEquals(moves[i].positions, [2, 1]) && moves[i].player === "2"):
          array[7] = 'O'
          break;
        case (this.arrayEquals(moves[i].positions, [2, 2]) && moves[i].player === "2"):
          array[8] = 'O'
          break;
      }
    }
  }
  get player(): string {
    return this.xIsNext ? 'X' : 'O';
  }

  makeMove(idx: number): void {
    if (!this.squares[idx] && !this.winner) {
      this.squares.splice(idx, 1, this.player);
      this.gameService.addMove(this.boardId,
        {player : this.currentPlayerNum, positions :this.numberToPosition(idx)}).subscribe((response)=>{
          this.shared.updateMoveList(this.squares);
          this.init();
      })
      this.xIsNext = !this.xIsNext;
      if(this.currentPlayerNum ==="1"){
        this.currentPlayerNum = "2";
        this.currentPlayer = this.player2Name;
      } else if(this.currentPlayerNum==="2"){
        this.currentPlayerNum = "1";
        this.currentPlayer = this.player1Name;
      }
      console.log("Pozicioni mbas levizjes"+ this.moves);
    }
    this.winner = this.calculateWinner();
    if(this.winner){
      this.gameService.endGame(this.boardId).subscribe()
    }

  }

  numberToPosition(num: number){
   switch (num){
      case 0:
        return [0,0]
      case 1:
        return [0,1]
      case 2:
        return [0,2]
      case 3:
        return [1,0]
      case 4:
        return [1,1]
      case 5:
        return [1,2]
      case 6:
        return [2,0]
      case 7:
        return [2,1]
      case 8:
        return [2,2]
     default:
       return []
    }

  }

  calculateWinner(): string {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
    ];
    for (const line of lines) {
      const [a, b, c] = line;
      if (
        this.squares[a] &&
        this.squares[a] === this.squares[b] &&
        this.squares[a] === this.squares[c]
      ) {
        return this.squares[a];
      }
    }
    return '';
  }
   arrayEquals(a:any, b:any) {
    return Array.isArray(a) &&
      Array.isArray(b) &&
      a.length === b.length &&
      a.every((val, index) => val === b[index]);
  }

  leaveGame() {
    this.gameService.endGame(this.boardId).subscribe(res =>{
      this.router.navigate(['../name'], {relativeTo: this.route})
    });

  }

  ngOnDestroy(): void {
    if(this.getFirstMoveSubscription){
      this.getFirstMoveSubscription.unsubscribe()
    }
    if(this.getTemporalArray){
      this.getTemporalArray.unsubscribe();
    }
  }

}

