import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Game} from "./models/game";
import {Movement} from "./models/movement";

@Injectable()
export class GameService {

  constructor(private http: HttpClient) {
  }
  tempArray : Movement[] = [{
    player: '',
    positions: []
  }];

  realArray : Movement[] = [{
    player: '',
    positions: []
  }];

  BaseUrl = "http://localhost:8080/";
  headers = new HttpHeaders({
    'Content-Type': 'application/json'

  });

  getAllGames(): Observable<any> {
    return this.http.get(this.BaseUrl + 'games/all', {headers: this.headers});
  }
  createGame(game: any): Observable<any> {
    return this.http.post(this.BaseUrl +'games/create', game ,{headers: this.headers});
  }

  getGame(id: number): Observable<any>{
    return this.http.get(this.BaseUrl +'games/find' +id, {headers: this.headers});
  }
  getCurrentGame(): Observable<any> {
    return this.http.get(this.BaseUrl +'games/current', {headers: this.headers})
  }
  addMove(id: string, movement: Movement ){
    return this.http.post(`${this.BaseUrl}games/add/${id}`, movement, {headers: this.headers});
  }

  endGame(id: string){
    return this.http.put(`${this.BaseUrl}games/finish/${id}`,{}, {headers: this.headers});
  }


  updateTempArray(val:Movement[], ind: number){
    this.tempArray = val.slice(0, ind)
  }

  updateRealArray(val:Movement[]){
    this.realArray = val;
  }

}
