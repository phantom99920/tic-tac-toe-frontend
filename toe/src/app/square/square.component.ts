import {Component, Input, OnInit} from '@angular/core';


@Component({
  selector: 'app-square',
  templateUrl: './square.component.html',
  styleUrls: ['./square.component.css']
})
export class SquareComponent implements OnInit {


  @Input() value: 'X' | 'O' | undefined;
  @Input() locked: boolean= false;
  constructor( ) { }

  ngOnInit(): void {
  }

  // changePlayer(){
  //   this.gameService.isGameRunning = true;
  //
  //   if (this.gameService.isGameRunning && this.square.state === null){
  //     this.square.state = this.gameService.activePlayer;
  //     this.gameService.changePlayerTurn(this.square);
  //   }
  // }

}
