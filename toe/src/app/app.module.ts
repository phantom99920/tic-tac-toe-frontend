import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from "@angular/common/http";
import { AppComponent } from './app.component';
import { BoardComponent } from './board/board.component';
import { SquareComponent } from './square/square.component';
import { LogsComponent } from './logs/logs.component';
import {GameService} from "./game.service";
import {FormsModule} from "@angular/forms";
import { EntryComponent } from './entry/entry.component';
import { AppRoutingModule } from './app-routing.module';
import {ReactiveFormsModule} from "@angular/forms";
import {SharedService} from "./shared/shared.service";

@NgModule({
  declarations: [
    AppComponent,
    BoardComponent,
    SquareComponent,
    LogsComponent,
    EntryComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    GameService,
    SharedService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
