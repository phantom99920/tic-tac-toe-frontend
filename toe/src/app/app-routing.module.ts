import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {EntryComponent} from "./entry/entry.component";
import {BoardComponent} from "./board/board.component";

const routes: Routes = [
  {path: 'name', component: EntryComponent},
  {path: 'game-board', component: BoardComponent},
  {
    path: '**',
    redirectTo: 'name',
    pathMatch: 'full'
  }
]


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
