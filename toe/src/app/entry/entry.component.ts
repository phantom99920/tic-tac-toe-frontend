import {Component, OnInit, ViewChild} from '@angular/core';
import {GameService} from "../game.service";
import {Router} from "@angular/router";
import {FormControl, FormGroup} from "@angular/forms";
import {SharedService} from "../shared/shared.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.css']
})
export class EntryComponent implements OnInit {

  player1Name: string = "Player1";
  player2Name: string = "Player2";
  button1: string = "1";
  button2: string = "2";
  isXfirst: boolean = true;
  resp = ''


  @ViewChild('f') enterForm: any;

  gameForm = new FormGroup( {
    player1Name: new FormControl('Player1'),
    player2Name: new FormControl('Player2'),
    turn: new FormControl(this.button1)
  })

  constructor(
    private gameService: GameService,
    private router: Router,
    private sharedService: SharedService
  ) {}

  ngOnInit(): void {
    this.gameService.getCurrentGame().subscribe((response: any) =>{
      this.resp = response
      if(this.resp && this.resp.length > 0){
        this.router.navigate(['/game-board']);
      } else {

      }
      this.resp = ''
    });
  }

  // swapPositions(): void {
  //   if(this.button1 =="1"){
  //     this.button1 = "2"
  //     this.button2 = "1"
  //   }  else {
  //     this.button1 = "1"
  //     this.button2 = "2"
  //   }
  // }

  onSelectionChange(str: string){
    this.isXfirst = str === "1";
  }

  onSubmit() {
    console.log(this.gameForm.value);
    this.gameService.createGame(this.gameForm.value).subscribe((response: any)=>{
        this.sharedService.updateStartingPlayer(this.gameForm.controls.turn.value);
      this.router.navigate(['/game-board']);
    })
  };

}
